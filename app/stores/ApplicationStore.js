import BaseStore from 'fluxible/addons/BaseStore';
import routesConfig from '../configs/routes';
import RouteStore from './RouteStore';

class ApplicationStore extends BaseStore {

  static storeName = 'ApplicationStore';
  static handlers = {
    'NAVIGATE_START': 'handleNavigateStart',
    'NAVIGATE_SUCCESS': 'handleNavigateSuccess',
  };

  constructor(dispatcher) {
    super(dispatcher);
    this.currentPageName = null;
    this.currentPage = null;
    this.pages = routesConfig;
    this.pageTitle = '';
  }

  handleNavigateStart(){
    this.pageTitle = 'loading....';
    this.emitChange();
  }
  handleNavigateSuccess(currentRoute){
    this.dispatcher.waitFor(RouteStore, () => {
      if (currentRoute && currentRoute.get('title')) {
        this.currentPageName = currentRoute.get('page');
        this.pageTitle = currentRoute.get('title');
        this.emitChange();
      }
    });
  }
  getCurrentPageName(){
    return this.currentPageName;
  }
  getPageTitle(){
    return this.pageTitle;
  }
  getPages(){
    return this.pages;
  }
  dehydrate(){
    return {
      currentPageName: this.currentPageName,
      currentPage: this.currentPage,
      pages: this.pages,
      pageTitle: this.pageTitle
    };
  }
  rehydrate(state){
    this.currentPageName = state.currentPageName;
    this.currentPage = state.currentPage;
    this.pages = state.pages;
    this.pageTitle = state.pageTitle;
  }
}

export default ApplicationStore;
