import React from 'react';
import ApplicationStore from '../stores/ApplicationStore';

class Html extends React.Component {

    static defaultProps = {
        script: [],
        css: []
    }

    render() {

        const {scripts, styles} = this.props;

        return (
            <html>
            <head>
                <meta charSet="utf-8" />
                <title>{this.props.context.getStore(ApplicationStore).getPageTitle()}</title>
                <meta name="viewport" content="width=device-width, user-scalable=no" />
                <link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
                { styles.map((href, k) =>
                    <link key={k} rel="stylesheet" type="text/css" href={'/public/css/' + href} />)
                }
            </head>
            <body className="pdg-0 mrg-0">
                <div id="app" className="wrap" dangerouslySetInnerHTML={{__html: this.props.markup}}></div>
            </body>
            <script dangerouslySetInnerHTML={{__html: this.props.state}}></script>
            { scripts.map((href, k) => <script key={k} src={'/public/js/' + href} />) }
            </html>
        );
    }
}

export default Html;
