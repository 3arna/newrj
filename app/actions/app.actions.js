import lodash from 'lodash';

export default {

  home(context, route, done){
	  context.service.read('photos', {}, {}, (err, tree) => {
	    context.dispatch('TREE_LOADED', tree);
	    done();
	  });
	},

	album(context, payload, done){
    context.service.read('photos', {}, {}, (err, tree) => {
      let selectedSet = lodash.find(tree[0].set, (set) => {
        return set.id == payload.get('params').get('id');
      });
      context.dispatch('ALBUM_LOADED', selectedSet);
      done();
    })
  },

  addItem(context, payload, done){
  	payload.item = payload.item || 'new item';
  	context.dispatch('ITEM_ADD', payload.item);
  	done();
  },

  removeItem(context, payload, done){
  	context.dispatch('ITEM_REMOVE', payload.index);
  	done();
  }

};