/*globals document*/

import React from 'react/addons';
import Nav from './components/Nav';
import Loading from './components/Loading';
import { connectToStores, provideContext } from 'fluxible-addons-react';
import { handleHistory } from 'fluxible-router';

@handleHistory
@provideContext
@connectToStores(['ApplicationStore'], (context, props) => {
  const appStore = context.getStore('ApplicationStore');
  return {
    currentPageName: appStore.getCurrentPageName(),
    pageTitle: appStore.getPageTitle(),
    pages: appStore.getPages(),
    isNavigateComplete: props.isNavigateComplete,
  }
})

class Application extends React.Component {
  render(){

    let Handler = Loading;

    if(this.props.isNavigateComplete){
      Handler = this.props.currentRoute.get('handler');
    }

    return (
      <div className="tb w-100 md-txt-center pdg-0 h-100">
        <div className="td md-dsp-block w-min-300px md-w-auto">
          <Nav selected={this.props.currentPageName} links={this.props.pages} />
        </div>
        <div className="td md-dsp-block md-pdg-0 w-100">
          <div className="boxshadow-10 bg-grey mrg-tb-20em">
            <Handler/>
          </div>
        </div>
      </div>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    const newProps = this.props;
    if (newProps.pageTitle === prevProps.pageTitle) {
      return;
    }
    document.title = newProps.pageTitle;
  }
}

export default Application;
