var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var webpackConfig = {
  resolve: {
    extensions: ['', '.js']
  },
  entry: [
    './app/client.js'
  ],
  output: {
    path: path.resolve('build'),
    publicPath: '/public',
    filename: '/js/main.min.js'
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loaders: [
          require.resolve('babel-loader')
        ]
      },
      { test: /\.json$/, loader: 'json-loader'},
      { test: /\.scss$/, loader: ExtractTextPlugin.extract('css!sass') },
    ]
  },
  plugins: [
    new ExtractTextPlugin('/css/main.min.css', {
      compress: true,
      allChunks: true
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  ],
  devtool: 'source-map'
};

module.exports = webpackConfig;
