import { BaseStore } from "fluxible/addons";

class ItemStore extends BaseStore {

  static storeName = 'ItemStore'

  static handlers = {
    'ITEM_ADD': 'handleAdd',
    'ITEM_REMOVE': 'handleRemove',
  }

  constructor(dispatcher){
    super(dispatcher);
    this.items = ['hello', 'world', 'click', 'me'];
  }

  handleAdd(item){
    this.dispatcher.waitFor(['ApplicationStore'], () => {

      this.items.push(item);
      this.emitChange();
    });
  }

  handleRemove(index){
    this.dispatcher.waitFor(['ApplicationStore'], () => {
      this.items = this.items.splice(index, 1);
      this.emitChange();
    });
  }

  getItems(){
    return this.items;
  }

  dehydrate(){
    return {
      items: this.items
    };
  }

  rehydrate(state){
    this.items = state.items;
  }

}


export default ItemStore;
