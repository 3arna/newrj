import React from 'react';
import { NavLink } from 'fluxible-router';
import { connectToStores } from 'fluxible-addons-react';

@connectToStores(['TreeStore'], (context, props) => (
  {
    tree: context.getStore('TreeStore').getTree(),
    selected: props.selected,
    links: props.links,
  }
))

class Nav extends React.Component {
    render(){
      const {selected, links, tree} = this.props;

      let treeMenu = false;

      const subMenu = Object.keys(links).map((name) => {

        let link = links[name];
        if(!link.navigation) return false;

        return (
          <NavLink className="pdg-20px clr-white txt-deco-none fix-bb dsp-inline" routeName={link.page} activeClass="bg-black">
            {link.title}
          </NavLink>
        );
      });

      treeMenu = tree.length && tree.map((collection) => {
        return(
          <div className="pdg-10px clr-white txt-deco-none fix-bb dsp-inline">{collection.title}</div>
        )
      });

      return (
        <div className="bg-blackl2 fnt-lg">
          <div className="wrap tb">
            <div className="td">
              <NavLink routeName="home" className="td clr-white txt-deco-none txt-center dsp-block pdg-20px" activeClass="bg-black">
                Home RJ
              </NavLink>
            </div>
            <div className="td txt-right">
              {treeMenu}
              {subMenu}
            </div>
          </div>
        </div>
      );
    }
}

Nav.defaultProps = {
    selected: 'home',
    links: {}
};

export default Nav;
