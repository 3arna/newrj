var webpack = require('webpack');
var path = require('path');

var webpackConfig = {
  resolve: {
    extensions: ['', '.js']
  },
  entry: [
    'webpack-dev-server/client?private-c9-noneede.c9.io:8080',
    'webpack/hot/only-dev-server',
    './client.js',
  ],
  output: {
    path: path.resolve('./build/'),
    publicPath: '/js/',
    filename: 'main.js'
  },
  module: {
    loaders: [
      {
        exclude: /node_modules/,
        loaders: [
          require.resolve('react-hot-loader'),
          require.resolve('babel-loader')
        ]
      },
      { test: /\.json$/, loader: 'json-loader'},
      { test: /\.scss$/, loader: 'style!css!sass'}
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    })
  ],
  devtool: 'eval,source-map'
};

module.exports = webpackConfig;
