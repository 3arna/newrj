import { BaseStore } from "fluxible/addons";

class AlbumStore extends BaseStore {

  static storeName = "AlbumStore"

  static handlers = {
    'ALBUM_LOADED': 'handleAlbum'
  }

  constructor(dispatcher){
    super(dispatcher);
    this.album = {};
  }

  handleAlbum(album) {
    this.dispatcher.waitFor(['ApplicationStore'], () => {
      if(this.album && this.album.id == album.id){
        return true;
      }
      this.album = album;
      this.emitChange();
    });
  }

  getAlbum(){
    return this.album;
  }

  dehydrate(){
    return {
      album: this.album
    };
  }

  rehydrate(state){
    this.album = state.album;
  }

}


export default AlbumStore;
