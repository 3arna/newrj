import React from 'react/addons';
import {addItem, removeItem} from '../../actions/app.actions';
import {connectToStores} from 'fluxible-addons-react';

@connectToStores(['ItemStore'], (context, props) =>
  ({ list: context.getStore('ItemStore').getItems()})
)

class Items extends React.Component {

  /*static defaultProps = {
    list: ['hello', 'world', 'click', 'me']
  }*/

  static contextTypes = {
    executeAction: React.PropTypes.func.isRequired
  }
  componentDidMount(){
    //alert('tadam');
    //this.setState({ mounted: true });
  }

  handleAdd(){
    var newItems = this.state.items.concat([]);
    this.setState({items: newItems});
  }

  addOneItem(){
    context.executeAction(addItem, {item: prompt('Enter some text')});
  }

  removeOneItem(index){
    
    context.executeAction(removeItem, {index: index});
  }

  render() {
    const Transition = React.addons.CSSTransitionGroup;
    const {list} = this.props;

    const items = list.map(function(item, i) {
      return (
        <div className="pdg-5em bg-white mrg-b-1em" key={i} onClick={this.removeOneItem.bind(this, i)}>
          {item}
        </div>
      );
    }.bind(this));
    return (
      <div className="pdg-30em txt-center">
        <button onClick={this.addOneItem} className="pdg-30em bg-blackl3 clr-white brd-0 mrg-b-20em">click me and Add Item</button>
        <Transition transitionName="item" transitionAppear={true}>
          {items}
        </Transition>
      </div>
    );
  }
}

export default Items;
