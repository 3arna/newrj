import React, { PropTypes } from 'react';
import { connectToStores } from 'fluxible-addons-react';

@connectToStores(['AlbumStore'], (context, props) =>
  ({ set: context.getStore('AlbumStore').getAlbum()})
)

class Photoset extends React.Component {
    render(){
        const {set} = this.props;

        let photoHtml = '';
        if(set.photo){
            photoHtml = set.photo.map((photo) => {
              return <img className="dsp-block w-100 pdg-rl-10em pdg-t-10em fix-bb" src={photo.url}/>
            });
        }

        return (
          <article className="mrg-b-20em pdg-tb-10em boxshadow-10 bg-white z-10 pos-relative">
            <h1 className="mrg-0 pdg-rl-20px fnt-lg txt-up pdg-t-20em">{set.title}</h1>
            <p className="mrg-0 mrg-tb-20em pdg-rl-20px">{set.description}</p> 
            {photoHtml}
          </article>
        )
    }
};

export default Photoset;
