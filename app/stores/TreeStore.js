import { BaseStore } from 'fluxible/addons';
import lodash from 'lodash';

class TreeStore extends BaseStore {

  static storeName = 'TreeStore'

  static handlers = {
    'TREE_LOADED': 'handleTree'
  }

  constructor(dispatcher) {
    super(dispatcher);
    this.tree = {};
    this.randomSets = [];
  }

  handleTree(tree) {
    this.dispatcher.waitFor([], () => {
      if(this.tree && Object.keys(this.tree).length){
        return true;
      }

      this.randomSets = this.randomiseSets(tree);
      this.tree = tree;
      this.emitChange();
    });
  }

  getTree(){
    return this.tree;
  }

  getRandomSets(){
    return this.randomSets;
  }

  randomiseSets(tree){
    return lodash.chain(tree)
      .clone(true)
      .first()
      .get('set')
      .shuffle()
      .slice(0, 3)
      .map((set) => {
        set.photo = lodash.chain(set.photo).first().get('url').value();
        return set;
      }).value();
  }

  dehydrate(){
    return {
      tree: this.tree,
      randomSets: this.randomSets,
    };
  }

  rehydrate(state){
    this.tree = state.tree;
    this.randomSets = state.randomSets;
  }

}


export default TreeStore;
