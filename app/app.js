import Fluxible from 'fluxible';
import fetchrPlugin from "fluxible-plugin-fetchr";
import Application from './views/Application';
import ApplicationStore from './stores/ApplicationStore';
import RouteStore from './stores/RouteStore';
import AlbumStore from './stores/AlbumStore';
import TreeStore from './stores/TreeStore';
import ItemStore from './stores/ItemStore';

// create new fluxible instance
const app = new Fluxible({
    component: Application,
    stores: [RouteStore, ApplicationStore, AlbumStore, TreeStore, ItemStore]
});

app.plug(fetchrPlugin({ xhrPath: "/api" }));

module.exports = app;
