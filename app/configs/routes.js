import {home, album} from '../actions/app.actions';
import Items from '../views/pages/Items';
import About from '../views/pages/About';
import Home from '../views/pages/Home';
import Album from '../views/pages/Album';

export default {
  home: {
    path: '/',
    method: 'get',
    page: 'home',
    title: 'Home',
    handler: Home,
    action: home,
  },
  about: {
    path: '/about',
    method: 'get',
    page: 'about',
    title: 'About',
    navigation: true,
    handler: About,
  },
  items: {
    path: '/items',
    method: 'get',
    page: 'items',
    title: 'Items',
    navigation: true,
    handler: Items,
  },
  photos: {
    method: 'get',
    page: 'photos',
    path: '/photos/:id',
    title: 'Photos',
    handler: Album,
    action: album,
  },
  album: {
    method: 'get',
    page: 'album',
    path: '/album/:id',
    title: 'Album',
    handler: Album,
    action: album,
  }
};
