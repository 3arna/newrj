import React from 'react';

class About extends React.Component{

  render() {
    return (
      <div className="pdg-30em txt-center">
        <h2>About</h2>
        <p>
        	I was not born knowing that I will be a photographer.
					I did not accidentally find an old film camera in the old grandpa‘s cabinet too.
					My story is not fancy. I just always wanted to live my life, not the life of others. The thing that I am holding a camera know, I would call a pure coincidence. But a really happy one.
					Photography for me is like the most wonderful excuse to know people I otherwise would have never met, to see places, where I have never been before and to tell stories about all that.
					My name is Rokas Jundulas,
					And I am a photographer.
				</p>
      </div>
    );
  }
}

export default About;
