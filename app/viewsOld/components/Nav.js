import React from 'react';
import { NavLink } from 'fluxible-router';
import { connectToStores } from 'fluxible-addons-react';

@connectToStores(['TreeStore'], (context, props) => (
  {
    tree: context.getStore('TreeStore').getTree(),
    selected: props.selected,
    links: props.links,
  }
))

class Nav extends React.Component {
    render() {
        const {selected, links, tree} = this.props;

        const subMenu = Object.keys(links).map((name) => {
            let className = 'bg-greyd3';
            let link = links[name];

            if (selected === name) {
                className = 'bg-black';
            }
            if(!link.navigation) return false;

            return (
                <NavLink className={'mrg-t-50em pdg-10em dsp-block clr-white txt-deco-none fix-bb ' + className} routeName={link.page}>
                    <li key={link.path}>
                        {link.title}
                    </li>
                </NavLink>
            );
        });

        const treeMenu = tree.length ? tree.map((collection) => {
            return(
                <ul key={collection.id} className="list-none op-8 pdg-0">
                    
                        <NavLink className="txt-deco-none op-8" routeName="home" activeClass="bg-black">
                            <li className="clr-white pdg-10em fnt-lg txt-up fix-bb bg-red">{collection.title}</li>
                        </NavLink>
                    
                    {collection.set.map((set) => {
                        let bgClass = 'bg-greyd2';

                        return (
                            <NavLink 
                                className={'txt-deco-none dsp-block clr-white pdg-tb-5em pdg-rl-10em fix-bb mrg-t-1em ' + bgClass} 
                                routeName="album" 
                                navParams={{id: set.id}} 
                                activeStyle={{backgroundColor: '#000000'}}>
                                    
                                    <li key={set.id}>
                                        {set.title}
                                    </li>
                                    
                            </NavLink>
                        )
                    })}
                </ul>
            )
        }) : false;

        return (
            <ul className="pos-fixed top-0 w-300px pdg-0 mrg-t-50em list-none txt-right md-pos-relative md-w-100">
                {treeMenu}
                {subMenu}
            </ul>
        );
    }
}

Nav.defaultProps = {
    selected: 'home',
    links: {}
};

export default Nav;
