/**
 * This leverages Express to create and run the http server.
 * A Fluxible context is created and executes the navigateAction
 * based on the URL. Once completed, the store state is dehydrated
 * and the application is rendered via React.
 */

import express from 'express';
import compression from 'compression';
import path from 'path';
import serialize from 'serialize-javascript';
import {navigateAction} from 'fluxible-router';
import debugLib from 'debug';
import csurf from "csurf";
import React from 'react';
import app from './app/app';
import HtmlComponent from './app/views/Html';
import { createElementWithContext } from 'fluxible-addons-react';

import photoService from './app/services/photos';

const htmlComponent = React.createFactory(HtmlComponent);
const env = process.env.NODE_ENV;

const debug = debugLib('chat-example');

const server = express();
server.use(express.static(path.join(__dirname, './build')));
//server.use('/public', express.static(path.join(__dirname, '/build')));
server.use(compression());




//server.use(csurf({ cookie: true }));

const fetchr = app.getPlugin("FetchrPlugin");

//registering services;
fetchr.registerService(photoService);

server.use(fetchr.getXhrPath(), fetchr.getMiddleware());

server.use((req, res, next) => {
    
    let context = app.createContext({
        req: req
    });

    debug('Executing navigate action');
    context.getActionContext().executeAction(navigateAction, {
        url: req.url
    }, (err) => {
        if (err) {
            return next(err);
        }
        
        debug('Exposing context state');
        const exposed = 'window.App=' + serialize(app.dehydrate(context)) + ';';

        debug('Rendering Application component into html');
        console.log(env);
        const html = React.renderToStaticMarkup(htmlComponent({
            scripts: env === 'production' ? ['main.min.js'] : ['main.js'],
            styles: env === 'production' ? ['main.min.css'] : [],
            context: context.getComponentContext(),
            state: exposed,
            markup: React.renderToString(createElementWithContext(context))
        }));
        debug('Sending markup');
        res.type('html');
        res.write('<!DOCTYPE html>' + html);
        res.end();
    });
});

const port = process.env.PORT || 3000;
server.listen(port);
console.log('Application listening on port ' + port);

export default server;
