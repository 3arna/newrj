import superagent from 'superagent';
import lodash from 'lodash';
import config from '../configs/flickr';
import moment from 'moment';
import async from 'async';

export default {

  get(options, done){

    lodash.isString(options) ? options = {method: options} : false;

    superagent.get(config.url)
      .query(this.combineQuery(options))
      .end((err, res) => {
        if(err) return done(err);

        switch(options.method){
          case 'tree':
            res.body = res.body.collections.collection.shift().collection;

            //load tree photosets photos
            options.combined ? this.photoTreeWithPhotos(res.body).then(()=>{done(null, res.body)}) : done(err, res.body);;
          break;
          case 'setPhotos':
            res.body = res.body.photoset;
            res.body.photo.forEach((photo) => {

              //count photoset views
              !res.body.views ? res.body.views = parseInt(photo.views) : res.body.views+= parseInt(photo.views);
              
              //set newest photoset date
              !res.body.date 
                ? res.body.date = parseInt(photo.dateupload+'000') 
                : res.body.date > parseInt(photo.dateupload) ? res.body.date = parseInt(photo.dateupload+'000') : false;

              this.photoDataToUrl(photo)
            });
            return done(err, res.body);
          break;
        }
    });
  },

  combineQuery(options){
    let query = {
      method: config.methods[options.method],
      api_key: config.key,
      format: 'json',
      nojsoncallback: 1
    };

    switch(options.method){
      case 'tree':
        query.user_id = config.user;
      break;
      case 'setPhotos':
        query.photoset_id = options.setId,
        query.extras = 'views,date_upload'
    }
    return query;
  },

  photoTreeWithPhotos(tree){
    return new Promise((resolve, reject) => {
      tree.forEach((collection) => {
        async.each(collection.set, (set, done) => {
          this.get({method: 'setPhotos', setId: set.id}, (err, photos) => {
            lodash.merge(set, photos);
            done(err);
          });
        }, (err) => {
          if(err) return reject(err);
          return resolve();
        });
      });
    });
  },

  photoDataToUrl(photo){
    photo.url = `https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_${config.size}.jpg`;
    return true;
  }

};
