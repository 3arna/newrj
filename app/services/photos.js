import Flickr from '../utils/flickrUtils';
import debug from 'debug';

export default {
  name: "photos",

  read(req, resource, options, config, done) {
    console.log('loading tree');
    if(req.app.tree) return done(null, req.app.tree);

    console.log('Flickr ->', 'loading new tree');
    Flickr.get({method: 'tree', combined: true}, (err, tree) => {
    	err || !tree.length ? console.log('Flickr ->', err, 'can not load photo tree') : false;
      //console.log(tree[0].set);
    	req.app.tree = tree;
    	done(err, tree);
    });
  }

};
