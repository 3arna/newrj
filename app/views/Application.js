/*globals document*/

import React from 'react/addons';
import Nav from './components/Nav';
import Loading from './components/Loading';
import { connectToStores, provideContext } from 'fluxible-addons-react';
import { handleHistory } from 'fluxible-router';

@handleHistory
@provideContext
@connectToStores(['ApplicationStore'], (context, props) => {
  const appStore = context.getStore('ApplicationStore');
  return {
    currentPageName: appStore.getCurrentPageName(),
    pageTitle: appStore.getPageTitle(),
    pages: appStore.getPages(),
    isNavigateComplete: props.isNavigateComplete,
  }
})


class Application extends React.Component {
  render(){

    let Handler = Loading;

    if(this.props.isNavigateComplete){
      Handler = this.props.currentRoute.get('handler');
    }

    return (
      <div>
        <Nav links={this.props.pages} selected={this.props.currentPageName}/>
        <header className="txt-center pdg-tb-50em">
          <h2 className="txt-up clr-blackl3 pdg-0 mrg-0">Rokas Jundulas</h2>
          <p className="pdg-0 mrg-0">photographer ... </p>
        </header>
        <div className="boxshadow-10 bg-grey mrg-tb-20em wrap">
          <Handler/>
        </div>
      </div>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    const newProps = this.props;
    if (newProps.pageTitle === prevProps.pageTitle) {
      return;
    }
    document.title = newProps.pageTitle;
  }
}

export default Application;
