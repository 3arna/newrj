import React from 'react/addons';

class Loading extends React.Component {
  render(){
  	const Transition = React.addons.CSSTransitionGroup;
    return (
      <div className="bg-white txt-center">
        <img src="http://www.brainpecks.com/wp-content/uploads/2014/02/clock-loading.gif"/>
      </div>
    )
  }
}

export default Loading;
