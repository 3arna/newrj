import React from 'react/addons';
import lodash from 'lodash';
import moment from 'moment';
import { connectToStores } from 'fluxible-addons-react';
import { NavLink } from 'fluxible-router';

@connectToStores(['TreeStore'], (context, props) => (
  {tree: context.getStore('TreeStore').getTree()}
))

class Home extends React.Component {
  render(){

    const {tree} = this.props;
    const tagClassNames = 'clr-redl2 pdg-rl-5em mrg-l-10em';
    const Transition = React.addons.CSSTransitionGroup;

    const contentHtml = tree.map((collection) => {
      return(
        collection.set.map((set) => {
          return (
            <NavLink className="txt-deco-none" routeName="album" navParams={{id: set.id}}>
              <Transition transitionName="item" transitionAppear={true}>
                <article className="album pdg-rl-20em pdg-t-20em pdg-b-30em z-10 pos-relative fix-clear brd-b-greyd1 brd-t-white brd-rl-white">
                  <div className="fix-clear pdg-t-20em">
                    <h1 className="mrg-0 fnt-lg txt-up flt-left md-flt-none">{set.title}</h1>
                    <div className="flt-right md-flt-none">
                      <span className={tagClassNames}><i className="fa fa-picture-o pdg-r-5em"></i>{set.total}</span>
                      <span className={tagClassNames}><i className="fa fa-eye pdg-r-5em"></i>{set.views}</span>
                      <span className={tagClassNames}><i className="fa fa-camera-retro pdg-r-5em"></i>{moment(set.date).fromNow()}</span>
                    </div>
                  </div>
                  <p className="mrg-0 mrg-tb-20em">
                    {lodash.trunc(set.description, 200)}
                  </p>

                  {set.photo.map((photo, index) => {
                    {if(index>=4){ return false;}}
                    return (
                      <div key={photo.id} className="flt-left col-3 sm-col-12 over-hidden fix-bb pdg-1em">
                        <div className="h-200px" style={{
                          backgroundImage: 'url(' + photo.url + ')',
                          backgroundPosition: 'center',
                          backgroundSize: 'auto 100%',
                          backgroundRepeat: 'no-repeat'
                        }}>
                        </div>
                      </div>
                    )
                  })}
                  <span className="bg-redl2 clr-white mrg-l-1em pdg-rl-10em txt-up">preview album</span>
                </article>
              </Transition>
            </NavLink>
          )
        })
      )
    });

    return (
      <div className="">
        <Transition transitionName="item" transitionAppear={true}>
          {contentHtml}
        </Transition>
      </div>
    );
  }
}

export default Home;
