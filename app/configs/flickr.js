// App config the for development environment.
// Do not require this directly. Use ./src/config instead.

export default {
	url: 'https://api.flickr.com/services/rest/',
	methods: {
		treePhotos: true,
		setPhotos: 'flickr.photosets.getPhotos',
		tree: 'flickr.collections.getTree',
	},
	key: '0eac0c9b06aa5c41e0fd61a2643131f6',
	user: '96622359@N06',
	size: 'b'
};
